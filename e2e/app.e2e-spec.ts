import { NgecleinPage } from './app.po';

describe('ngeclein App', () => {
  let page: NgecleinPage;

  beforeEach(() => {
    page = new NgecleinPage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to app!');
  });
});
