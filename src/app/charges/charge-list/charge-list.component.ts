import {Component, OnInit} from "@angular/core";
import {Model} from "../../model";

@Component({
  selector: 'app-charge-list',
  templateUrl: './charge-list.component.html',
  styleUrls: ['./charge-list.component.css'],
})
export class ChargeListComponent implements OnInit {
  private charge: Charge;
  readonly endpoint = 'certifications';

  constructor() {
  }

  ngOnInit() {
  }

}

export class Charge implements Model {
  id = '';
  title = '';
  description = '';

}
