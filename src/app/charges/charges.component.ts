import {Component, OnInit} from "@angular/core";
import {Model} from "../model";

@Component({
  selector: 'app-charges',
  templateUrl: './charges.component.html',
  styleUrls: ['./charges.component.css']
})
export class ChargesComponent implements OnInit {
  constructor() {
  }

  ngOnInit() {
  }

}

export class Charge implements Model {
  id = '';
  title = '';
  description = '';

}
