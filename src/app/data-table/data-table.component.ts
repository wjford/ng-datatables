import {Component, Input, OnInit} from "@angular/core";
import {ColumnComponent} from "../column/column.component";
import {BackendService} from "../backend.service";
import {APIResponse} from "./api-response";

@Component({
  selector: 'dataTable',
  templateUrl: './data-table.component.html',
  styleUrls: ['./data-table.component.css']
})
export class DataTableComponent implements OnInit {
  @Input() dataset;
  @Input() endPoint: string;
  @Input() title: string;
  @Input() filterField: string = '';

  data: APIResponse;
  pageBy = 10;
  filterString = '';
  page = 1;
  numPages = 1;
  pageArray = [];

  columns: ColumnComponent[] = [];

  addColumn(column) {
    this.columns.push(column);
    console.log(this.columns);
  }

  constructor(private backend: BackendService) {

  }

  fetchData(options: object) {
    this.backend.getAll(this.endPoint, options)
      .subscribe(data => {
        this.data = new APIResponse(data.json());
        this.dataset = this.data.results;
        this.numPages = Math.ceil(this.data.count / this.pageBy);
        this.setupPageArray();
      });
  }

  private setupPageArray() {
    this.pageArray = new Array(this.numPages);
    for (let i = 0; i < this.pageArray.length; i++) {
      this.pageArray[i] = i + 1;
    }
  }

  onChangeNumPerPage() {
    this.page = 1;
    this.updateFilter();
  }

  updateFilter() {
    const options = {
      page_by: this.pageBy,
      page: this.page
    };
    options[this.filterField] = this.filterString;

    this.fetchData(options);
  }

  ngOnInit() {
    this.fetchData({});
  }

  onPageClick(newPage) {
    if (newPage > 0 && newPage <= this.numPages) {
      this.page = newPage;
      this.updateFilter();
    }
    console.log(this.data);
  }

}
