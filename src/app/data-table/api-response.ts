export class APIResponse {
  hasNext: boolean;
  hasPrevious: boolean;
  count: number;
  results: object[];

  constructor(response: any) {
    this.hasNext = !!response.next;
    this.hasPrevious = !!response.previous;
    this.count = response.hasOwnProperty('count') ? response.count : 0;
    this.results = response.hasOwnProperty('results') ? response.results : [];
  }

}
