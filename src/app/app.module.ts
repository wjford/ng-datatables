import {BrowserModule} from "@angular/platform-browser";
import {NgModule} from "@angular/core";
import {RouterModule, Routes} from "@angular/router";
import {HttpModule} from "@angular/http";

import {AppComponent} from "./app.component";
import {BackendService} from "./backend.service";
import {DataTableComponent} from "./data-table/data-table.component";
import {ColumnComponent} from "./column/column.component";
import {ChargesComponent} from "./charges/charges.component";
import {ChargeListComponent} from "./charges/charge-list/charge-list.component";
import {FormsModule} from "@angular/forms";

const appRoutes: Routes = [];

@NgModule({
  declarations: [
    AppComponent,
    DataTableComponent,
    ColumnComponent,
    ChargesComponent,
    ChargeListComponent
  ],
  imports: [
    RouterModule.forRoot(appRoutes, {
      enableTracing: true
    }),
    BrowserModule,
    HttpModule,
    FormsModule
  ],
  providers: [BackendService],
  bootstrap: [AppComponent],
  exports: [DataTableComponent, ColumnComponent]
})
export class AppModule {
}
