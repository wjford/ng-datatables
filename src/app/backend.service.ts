import {Injectable} from "@angular/core";
import {Http} from "@angular/http";
import {environment} from "../environments/environment";
import {Observable} from "rxjs/Observable";

@Injectable()
export class BackendService {

  constructor(private http: Http) {
  }

  private formatQueryString(json): string {
    if (json) {
      return '?' +
        Object.keys(json).map(function (key) {
          return encodeURIComponent(key) + '=' +
            encodeURIComponent(json[key]);
        }).join('&');
    } else {
      return '';
    }
  }

  getAll(endPoint: string, params: object): Observable<any> {
    return this.http.get(environment.apiURL + endPoint + this.formatQueryString(params));
  }

}
